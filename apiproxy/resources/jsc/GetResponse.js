 function isEmpty(obj) {
    try {
        if (typeof obj == 'object') {
            if (Object.keys(obj).length > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            if ((obj !== null) && (obj !== undefined) && (obj !== "")) {
                return false;
            } else {
                return true;
            }
        }
    } catch (err) {
        return true;
    }
}

var getquickLinksDoc = context.getVariable("quickLinksDoc");

if (isEmpty(getquickLinksDoc)) {
    var response = {
        "data": [],
        "metadata": {
            "locale": "en-us"
        },
        "errors": null
    };
    context.setVariable("message.header.Content-Type", "application/json");
    context.setVariable("response.content", JSON.stringify(response));
}

if (!isEmpty(getquickLinksDoc)) {
    context.setVariable("message.header.Content-Type", "application/json");
    context.setVariable("response.content", (getquickLinksDoc));
}