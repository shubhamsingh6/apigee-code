 function isValidJSONString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

var json=context.getVariable("request.content");
var triggerError=false;
if(!isValidJSONString(json)){
 
triggerError=true;
}

    context.setVariable("triggerError",triggerError);